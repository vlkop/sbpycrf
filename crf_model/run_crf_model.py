import sys
sys.path.append('../text_process_utils')
sys.path.append('../feature_creation_utils')
import text_process_utils as tp
import feature_creation_utils as fc
import time
from sklearn.externals import joblib
import pandas as pd
import numpy as np

# TODO refactor that function!!!
def export_entities_to_csv(y_pred, X_test, y_test):
    predicted_tag = []
    value = []
    values = []
    probs = []
    probs_all = []
    real_number_all = []
    prev_test_ind = None
    prev_tag = None
    for i in range(len(y_pred)):
        crf.tagger_.set(X_test[i])
        for j in range(len(y_pred[i])):
            if prev_tag is not None and prev_tag != y_pred[i][j]:
                if prev_tag != '0':
                    #probs_all.append(';'.join(probs))
                    proba = round(np.mean(probs), 2)
                    if proba >= 0:
                        value.append(' '.join(values))
                        predicted_tag.append(prev_tag)
                        probs_all.append(round(np.mean(probs), 2))
                        real_number_all.append(y_test[prev_test_ind]['file_name'])
                probs = []
                values = []
            #probs.append(str(round(crf.tagger_.marginal(y_pred[i][j], j), 2)))
            probs.append(crf.tagger_.marginal(y_pred[i][j], j))
            values.append(X_test[i][j][1][11:])
            prev_tag = y_pred[i][j]
            prev_test_ind = i

    my_dict = {'predicted': predicted_tag, 'value': value, 'probs': probs_all, 'real_number': real_number_all}
    final_df = pd.DataFrame(my_dict)[['predicted', 'value', 'probs', 'real_number']]
    final_df.to_csv('predicted_values_train_clean_and_zeros_without_undef.csv', index=False, encoding='cp1251')
    return final_df


if __name__ == "__main__":
    print('program has started')
    start_time = time.time()
    print('obtaining data')
	# path to raw data to detect entities
    path = r''
    #data_sentences = tp.get_data(path)
    #data_formatted, tags = tp.format_tokens(data_sentences, is_train=False)
    data_formatted_all, tags = tp.get_sentences_for_test(path)
    '''
    with open("test.txt", "a") as myfile:
        for sent in data_formatted_all:
            myfile.write(str(sent))
            myfile.write('\n')
    '''
    #print(data_formatted_all)
    print("done")
    # convert data to feature set and labels set
    #X_data = [fc.sent2features(s['tokens']) for s in data_formatted]
    #y_data = [fc.sent2labels(s['tokens'], s['file_name']) for s in data_formatted]
    X_data = [fc.sent2features(s['tokens']) for s in data_formatted_all ]
    y_data = [fc.sent2labels(s['tokens'], s['file_name']) for s in data_formatted_all]
    crf = joblib.load('crf_model.pkl')
    print('entities extraction...')
    y_pred = crf.predict(X_data)
    print('done')
    export_entities_to_csv(y_pred, X_data, y_data)