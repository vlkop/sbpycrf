import sys
sys.path.append('../text_process_utils')
sys.path.append('../feature_creation_utils')
sys.path.append('../common_utils')
import text_process_utils as tp
import feature_creation_utils as fc
import common_utils as cu
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from hyperopt import fmin, tpe, Trials, hp, space_eval
import time
import sklearn_crfsuite
from sklearn.metrics import make_scorer
from sklearn_crfsuite import metrics
from sklearn.externals import joblib
from random import shuffle


def objective(params):
    crf.set_params(**params)
    shuffle = KFold(n_splits=2, shuffle=True)
    score = cross_val_score(crf, X_train, y_train, cv=shuffle, scoring=f1_scorer, n_jobs=4)

    return 1-score.mean()


if __name__ == "__main__":
    print('program has started')
    start_time = time.time()
    print('obtaining data')
	# path to training data
    path = r''
    #data_sentences = tp.get_data(path)
    #data_formatted_all, tags = tp.format_tokens(data_sentences)
    data_formatted_all, tags = tp.get_sentences(path)
    shuffle(data_formatted_all)
    print("done")
    print("following tags were obtained from documents:")
    print(tags)
    #train, test = cu.train_test_split_common([x for x in data_formatted_all if x['feature'] == 'clean' or (x['feature'] == 'by_step')], train_size=0.8)
    train, test = cu.train_test_split_common([x for x in data_formatted_all], train_size=0.8)

    with open("test.txt", "w") as myfile:
        for sent in [x for x in data_formatted_all if x['feature'] == 'by_step' and not x['all_zeros']]:
            myfile.write(str(sent))
            myfile.write('\n')


    # convert data to feature set and labels set
    '''
    X_train = [fc.sent2features(s['tokens']) for s in train if not s['all_zero_sentence']]
    y_train = [fc.sent2labels(s['tokens']) for s in train if not s['all_zero_sentence']]
    X_test = [fc.sent2features(s['tokens']) for s in test if not s['all_zero_sentence']]
    y_test = [fc.sent2labels(s['tokens']) for s in test if not s['all_zero_sentence']]
    X_data = [fc.sent2features(s['tokens']) for s in data_formatted_all if not s['all_zero_sentence']]
    y_data = [fc.sent2labels(s['tokens']) for s in data_formatted_all if not s['all_zero_sentence']]
    '''
    X_train = [fc.sent2features(s['sentence']) for s in train]
    y_train = [fc.sent2labels(s['sentence']) for s in train]
    X_test = [fc.sent2features(s['sentence']) for s in test]
    y_test = [fc.sent2labels(s['sentence']) for s in test]
    print('train set length: {length}'.format(length=len(y_train)))
    print('test set length: {length}'.format(length=len(y_test)))
    print('test set including zero sentences length: {length}'.format(length=len(test)))

    f1_scorer = make_scorer(metrics.flat_f1_score,
                            average='weighted', labels=list(tags))
    crf = sklearn_crfsuite.CRF()

    space = {'c1': hp.choice('c1', [0, 0.001]),
             'c2': hp.choice('c2', [0, 0.001])}

    trials = Trials()

    best = fmin(objective,
                space,
                algo=tpe.suggest,
                max_evals=2,
                trials=trials)
    best_params = space_eval(space, best)
    print('best parameters for a model:')
    print(best_params)
    # Fit the model with the optimal hyperparameters
    crf.set_params(**best_params)
    crf.fit(X_train, y_train)
    y_pred = crf.predict(X_test)
    print('report for clean and by_step test set:')
    print(metrics.flat_classification_report(y_test, y_pred, labels=list(tags), digits=3))

    '''
    X_test = [fc.sent2features(s['sentence']) for s in test if s['feature'] == 'by_step']
    y_test = [fc.sent2labels(s['sentence']) for s in test if s['feature'] == 'by_step']
    y_pred = crf.predict(X_test)

    print('report for by_step test set:')
    print(metrics.flat_classification_report(y_test, y_pred, labels=list(tags), digits=3))
    '''
    X_data = [fc.sent2features(s['sentence']) for s in data_formatted_all]
    y_data = [fc.sent2labels(s['sentence']) for s in data_formatted_all]

    print('done')
    print('saving model...')
    _ = joblib.dump(crf, 'crf_model.pkl', compress=9)
    print('done')
    print('program finish with time {finish_time}'.format(finish_time=time.time() - start_time))

    '''
    crf.fit(X_train, y_train)
    y_pred = crf.predict(X_test)
    print('report for clean test set:')
    print(metrics.flat_classification_report(y_test, y_pred, labels=list(tags), digits=3))

    X_test_data = [fc.sent2features(s['tokens']) for s in test]
    y_test_data = [fc.sent2labels(s['tokens']) for s in test]
    y_pred = crf.predict(X_test_data)
    print('report for test including zero sentences:')
    print(metrics.flat_classification_report(y_test_data, y_pred, labels=list(tags), digits=3))

    X_test_data = [fc.sent2features(s['tokens']) for s in data_formatted_all if s['all_zero_sentence']]
    y_test_data = [fc.sent2labels(s['tokens']) for s in data_formatted_all if s['all_zero_sentence']]
    y_pred = crf.predict(X_test_data)
    final_score = metrics.flat_accuracy_score(y_test_data, y_pred)

    print('percent of incorrectly predicted zero entity: {final_score}'.format(final_score=1-final_score))
    print('train model on all the data with best parameters...')
    crf.fit(X_data, y_data)
    print('done')
    print('saving model...')
    _ = joblib.dump(crf, 'crf_model.pkl', compress=9)
    print('done')
    print('program finish with time {finish_time}'.format(finish_time=time.time() - start_time))
    '''
