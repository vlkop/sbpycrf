import time
from itertools import chain
import nltk
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.preprocessing import LabelBinarizer
import sklearn
import pycrfsuite
import pandas as pd
import numpy as np
import os
from html.parser import HTMLParser
import re
import string
import pymorphy2
regex_tag = re.compile('(<\w\w\w+>[^<]+</\w\w\w+>)')  # рассчитано, что в теге как минимум три символа
morph = pymorphy2.MorphAnalyzer()


def get_pos_tag(word):
    if str(morph.parse(word)[0][4][0][0]) != '<FakeDictionary>' and morph.parse(word)[0][3] > 0.2:
        if morph.parse(word)[0].tag.POS is None:
            try:
                float(word.replace(',','.'))
                return 'NUMB'
            except ValueError:
                return 'undef'
        else:
            return morph.parse(word)[0].tag.POS
    else:
        return 'undef'

r'<\w+>(.+?)</\w+>'

'''
def get_data(path):
    sentences = []

    for file in os.listdir(path):
        if file[-3:] == 'txt':
            #with open('test.txt', 'w') as thefile:
            with open(r'{path}\{file_name}'.format(path=path, file_name=file), 'r') as f:
                lines = f.readlines()
                sentence = ''
                words_count = 0
                for line in lines:
                    //words_count += len(re.split('[^а-яА-Я0-9]+', line))
                    line = line.strip()
                    line = re.sub('\s+', ' ', line)
                    sentence = sentence + ' ' + line
                    if words_count >= 20:
                        sentences.append({'text': sentence, 'file_name': file[:-4]})
                        #thefile.write("%s\n'==============='\n" % sentence)
                        sentence = ''
                        words_count = 0
                #[sentences.append({'text': x, 'file_name': file[:-4]}) for x in nltk.sent_tokenize(re.sub( '\s+', ' ', f.read()).strip())]

                #print(len(sentences))
    return sentences

'''
'''
def get_data(path):
    sentences = []

    for file in os.listdir(path):
        if file[-3:] == 'txt':
            #with open('test.txt', 'w') as thefile:
            with open(r'{path}\{file_name}'.format(path=path, file_name=file), 'r') as f:
                lines = f.readlines()
                sentence = ''
                words_count = 0
                for line in lines:
                    words_count += len(re.split('[^а-яА-Я0-9]+', line))
                    line = line.strip()
                    line = re.sub('\s+', ' ', line)
                    sentence = sentence + ' ' + line
                    if words_count >= 20:
                        sentences.append({'text': sentence, 'file_name': file[:-4]})
                        #thefile.write("%s\n'==============='\n" % sentence)
                        sentence = ''
                        words_count = 0
                #[sentences.append({'text': x, 'file_name': file[:-4]}) for x in nltk.sent_tokenize(re.sub( '\s+', ' ', f.read()).strip())]

                #print(len(sentences))
    return sentences
'''


def get_data(path):
    sentences = []

    for file in os.listdir(path):
        if file[-3:] == 'txt':
            # with open('test.txt', 'w') as thefile:
            with open(r'{path}\{file_name}'.format(path=path, file_name=file), 'r') as f:
                sentences.append({'text': f.read(), 'file_name': file[:-4]})
    return sentences


def process_token(token):
    # Если это не теговый токен, то просто делим по словам, используя в качестве разделителя русские символы и цифры (ну и заодно удаляем все символы кроме русских и цифр)
    if len(re.findall(r'</\w+>', token)) == 0:
        words = []
        #print(token)
        token = re.sub('\s+', ' ', re.sub('[' + string.punctuation + ']', ' ', token.strip()))
        for word in re.split('[^а-яА-Я0-9]+', token):
            if word != ' ' and word != '':
                words.append((word, get_pos_tag(word), '0'))
        return words
    else: # Если это теговый токен, то разбиваем его по словам, каждому слову присваивая соответствующее значение сущности
          # и добавляем часть речи. Если слово непонятный бред, то часть речи будет равна 'undef'
        tag_name = re.findall(r'</\w+>', token)[0][2:-1].lower()  # важно к нижнему регистру привезти название тега
        tag_value = re.findall(r'<\w+>(.+?)</\w+>', token)[0]
        inner_words = []
        for word in tag_value.split():
            if word.lower() in ['вс', 'фс']:
                inner_words.append((word, 'NOUN', tag_name))
            elif word.lower() in ['№']:
                inner_words.append((word, 'NUMBER_SYMBOL', tag_name))
            else:
                pos_token = get_pos_tag(word)
                inner_words.append((word, pos_token, tag_name))
        return inner_words


def get_sentences(path):
    '''
    Вернет словарь, в котором будут предложения с токенами в формате (текст, часть речи, сущность), а также признаки
    какое это предложение (ключ "feature") идеальное  (значение 'clean') и набранное по 16 слов (значение 'by_step'),
    все ли сущности нулевые (ключ "all_zeros", значения True и False). Ну и еще найденные теги вернет

    :param path:
    :return:
    '''

    all_sentences = []
    tags = set()
    files_counter = 0
    for file in os.listdir(path):
        if file[-3:] == 'txt':
            # with open('test.txt', 'w') as thefile:
            with open(r'{path}\{file_name}'.format(path=path, file_name=file), 'r') as f:
                text = f.read()
                text_list = []
                sentences = []
                # Получаем токены (в формате (текст, часть речи, сущность)
                [text_list.extend(process_token(x)) for x in re.split('(<\w+>[^<]+</\w+>)', re.sub('\s+', ' ', text))]
                tag_value = []
                start_pos = None
                sentence = []
                # Проходимся по нашим токенам, создавая "идеальные" варианты последовательностей (идельной считается такая,
                # в которой внутри тега и в других словах нет нераспознанных слов, а также центром которой являются слова
                # ненулевой сущности, длина такой последовательности должна быть не меньше 6 слов слева и справа от тега)
                for i in range(len(text_list)):
                    words_left_right = 0
                    if text_list[i][2] != '0':
                        if start_pos is None:
                            start_pos = i
                        tag_value.append(str(text_list[i][1]))
                    elif start_pos is not None:
                        end_pos = i - 1
                        if 'undef' not in tag_value:
                            sentence.extend([x for x in text_list[start_pos:end_pos + 1]])
                            for i in range(1, 21):
                                try:
                                    if start_pos-i < 0:
                                        raise IndexError

                                    word_before = text_list[start_pos-i]
                                    word_after = text_list[end_pos+i]
                                    if str(word_before[1]) != 'undef' and str(word_after[1]) != 'undef':
                                        sentence.insert(0, word_before)
                                        sentence.append(word_after)
                                        words_left_right += 1
                                    else:
                                        break
                                except IndexError:
                                    break
                            if words_left_right >= 3:
                                sentences.append({'sentence': sentence, 'feature': 'clean'})
                            sentence = []

                        start_pos = None
                        tag_value = []

                [tags.add(x[2].lower()) for x in text_list if x[2] != '0']
                sentence = []

                partion = 16
                # добавляем неидеальные последовательности (около 16 слов и все слова нормально распознаны, в таких
                # предложениях могут быть как ненулевые сущности, так и нулевые)
                for i in range(len(text_list)):
                    sentence.append(text_list[i])
                    if text_list[i][2] == '0' and (len(sentence) > partion or i == len(text_list) - 1):
                        if len([x for x in sentence if str(x[1]) == 'undef']) == 0:
                            sentences.append({'sentence': sentence, 'feature': 'by_step', 'all_zeros': len([1 for x in sentence if x[2] != '0']) == 0})
                        sentence = []

                all_sentences.extend(sentences)
        if len(all_sentences) > 0:
            print('mean clean sentences length: ' + str(np.mean([len(x['sentence']) for x in all_sentences if x['feature'] == 'clean'])))
            print('mean by_step sentences length: ' + str(
                np.mean([len(x['sentence']) for x in all_sentences if x['feature'] == 'by_step'])))
            print('mean all sentences length: ' + str(
                np.mean([len(x['sentence']) for x in all_sentences ])))
        files_counter += 1
        print('processed files: ' + str(files_counter))

    return all_sentences, tags


def get_sentences_for_test(path):
    all_sentences = []
    tags = set()
    files_counter = 0
    for file in os.listdir(path):
        if file[-3:] == 'txt':
            # with open('test.txt', 'w') as thefile:
            with open(r'{path}\{file_name}'.format(path=path, file_name=file), 'r') as f:
                sentence = []
                for text in f.readlines():
                    text_list = process_token(re.sub('\s+', ' ', text))
                    for word in text_list:
                        sentence.append(word)
                    if len(sentence) > 16:
                        all_sentences.append({'tokens': sentence, 'file_name': file[:-4]})
                        sentence = []
        files_counter += 1
        print('processed files: ' + str(files_counter))

    return all_sentences, tags


def get_description(token, tags):
    """
    Function adds part of speech and entity id to token

    :param token: usually it is a word
    :param tags:
    :return:
    """
    to_return = []
    for tag in tags:
        reg_list = re.findall(r'<{tag}>(.+?)</{tag}>'.format(tag=tag), token.lower())
        if len(reg_list) == 1:
            for word in reg_list[0].split():
                to_return.append((word, nltk.pos_tag([word], lang='rus')[0][1], tag))
            return to_return
        elif len(reg_list) > 1:
            raise Exception('more than 1 tag in token')

    return [(re.sub(r'[^\w\s]', '', token), nltk.pos_tag([token], lang='rus')[0][1], '0')]


def get_formatted_tokens(tag_splitted_list, tags):
    """
    Function transforms tags splitted list to set of tokens in following format: [(token_text, part_of_speech, entity_tag)]

    :param tag_splitted_list: list of tokens obtained from sentence tag splitting: ['some text', '<my_tag>inner text</my_tag>', 'another text']
    :param tags: set of tags to detect
    :return:formatted_tokens_set: list with formatted tokens
    """

    tokens_list = []
    formatted_tokens_set = []
    # из листа с разбивкой по тегам те элементы, которые не являются тегами (и их содержимым) разобьем по словам, а которые являются - не трогаем
    [tokens_list.append(x) if len(regex_tag.findall(x)) > 0 else tokens_list.extend(
        re.split('[^а-яА-Я0-9]+', x)) for x in tag_splitted_list]  # TODO здесь удалил англ буквы. правильно ли это?

    # добавить описание к токенам
    for token in [x for x in tokens_list if x != '']:
        [formatted_tokens_set.append(x) for x in get_description(token, tags)]  # tokens_sent.append(get_tags(token, tags))
    return formatted_tokens_set


def format_tokens(sentences, is_train=True):
    """
    Functions transforms list of sentences to set of tokens in following format: [(token_text, part_of_speech, entity_tag)]

    :param sentences: sentences to format. List of strings.
    :param is_train: if train=True then format and return only sentences with entities else format and return all sentences
    :return: list of tokens in following format: [(token_text, part_of_speech, entity_tag)],
             list of tags (entities) that was detected in the sentences,
             list of tokens in following format : [(token_text, part_of_speech, entity_tag)] to test (only zero entities)
    """
    all_formatted_tokens = []
    all_formatted_tokens_test = []
    tags = set()
    for i in range(len(sentences)):
       # try:
        all_zero_sentence = True
        formatted_tokens_set = []
        for token in regex_tag.findall(sentences[i]['text']):
            tags.add(re.findall('<(\w+)>', token)[0].lower())
        tag_splitted = regex_tag.split(sentences[i]['text'])
        formatted_tokens_set = get_formatted_tokens(tag_splitted, tags)
        if len(tag_splitted) > 1:
            all_zero_sentence = False
        #except:
        #    continue
        if len(formatted_tokens_set) > 0:
            all_formatted_tokens.append({'tokens': formatted_tokens_set, 'file_name': sentences[i]['file_name'], 'all_zero_sentence': all_zero_sentence, 'index': i})
            #all_formatted_tokens_test.append({'tokens': formatted_tokens_set_test, 'file_name': sentence['file_name']})

    return all_formatted_tokens, tags



