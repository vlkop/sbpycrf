

def train_test_split_common(X, y=None, train_size=0.8):
    margin_index = round(len(X) * train_size)
    X_train = X[:margin_index]
    X_test = X[margin_index:]

    if y is not None:
        y_train = y[:margin_index]
        y_test = y[margin_index:]
        return X_train, X_test, y_train, y_test
    else:
        return X_train, X_test
